'use strict';

var path         = require('path')
  , webpack      = require('webpack')
  , node_modules = path.resolve(__dirname, 'node_modules')
  , config;

config = {
  target : 'web',
  cache  : true,
  entry  : {
    app : [ 'webpack-dev-server/client?http://0.0.0.0:8080', 'webpack/hot/only-dev-server', './app/app.js' ]
  },
  resolve : {
    modulesDirectories : [ 'node_modules', 'src' ]
  },
  output : {
    path     : path.join(__dirname, 'app'),
    filename : 'bundle.js'
  },
  module : {
    loaders : [
      { test : /\.js$/, loader : 'react-hot!babel', exclude : /node_modules/ }
    ]
  }
};

module.exports = config;
