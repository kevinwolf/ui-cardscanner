import React, { Component } from 'react';
import _ from 'lodash';
import CARD_CODES from '../constants/CardCodes';
import SHOE_CODES from '../constants/ShoeCodes';

const inputStyle = {
  left     : '-10000',
  position : 'absolute',
};

class CardScanner extends Component {

  static propTypes = {
    'onScan'       : React.PropTypes.func.isRequired,
    'onDealerScan' : React.PropTypes.func.isRequired,
    'onShoeScan'   : React.PropTypes.func.isRequired,
    'dealerList'   : React.PropTypes.array.isRequired,
  }

  componentDidMount () {
    this.refs.cardScanner.getDOMNode().focus();
  }

  render () {
    return (
      <input
        style={inputStyle}
        type="text"
        ref="cardScanner"
        onKeyDown={this._onKeyDown}
        onBlur={this._onBlur} />
    );
  }

  _onKeyDown = e => {
    if (e.keyCode === 13) {
      const code = this.refs.cardScanner.getDOMNode().value;
      this.refs.cardScanner.getDOMNode().value = '';
      if (SHOE_CODES[code]) {
        this.props.onShoeScan(SHOE_CODES[code]);
      } else if (CARD_CODES[code]) {
        this.props.onScan(CARD_CODES[code]);
      } else {
        this.props.onDealerScan((_.includes(this.props.dealerList, `${code}`)) ? `${code}` : 'NOT_FOUND');
      }
    }
  }

  _onBlur = () => {
    this.refs.cardScanner.getDOMNode().focus();
  }

}

export default CardScanner;
