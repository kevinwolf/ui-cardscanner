import React, { Component } from 'react';
import CardScanner from '../src/';

class ScannerTest extends Component {

  constructor (props) {
    super(props);
  }

  render () {
    return <CardScanner onScan={this._onScan} />
  }

  _onScan = card => {
    console.log('scanned ', card);
  }

}

React.render(<ScannerTest />, document.getElementById('componentTest'));
