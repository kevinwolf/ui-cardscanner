#CardScanner
Add this module on all *DealerInterfaces* which needs to be able to scan cards and send them back to a given socket.

##Usage
1. Run `npm install apogeelive.ui.cardScanner`.
2. Import the module on the application.
3. Set a callback function.

###Example
```javascript
import React, { Component }
import CardScanner from 'apogeelive.ui.cardScanner';

class App extends Component {

  render () {
    return <CardScanner onScan={this._onScan} />
  }

  _onScan = card => {
    console.log('scanned ', card);
  }

}

React.render(<App />, document.body);

```
